const arr = [];
// bài 9
const arr_9 = []
const newArr_9 = document.getElementById('addArr_9')
newArr_9.style.display = 'none'
document.getElementById('btn_9').onclick = function() {
    let inputBai9 = document.getElementById('addArr').value*1;
    arr_9.push(inputBai9)
    document.getElementById('newArr_9').innerHTML += `${inputBai9}, `
    console.log(arr_9);
}
// if user onclick option 6 
var chucNang = document.getElementById('yeuCau')
const bai6 = document.getElementById('bai_6')
bai6.style.display = 'none'
chucNang.onchange = function() {
    if (chucNang.value ==6) {
        bai6.style.display = 'block'
        newArr_9.style.display = 'none'
    } else if (chucNang.value == 9) {
        bai6.style.display = 'none'
        newArr_9.style.display = 'block'
    } else {
        bai6.style.display = 'none'
        newArr_9.style.display = 'none'
    }
}
// luu value user nhap 
document.getElementById('luuArr').onclick = function() {
    var value = document.getElementById('soArr').value*1;
    arr.push(value);
    console.log(arr);
    document.getElementById('showArr').innerHTML = arr
}


// Tính kết quả , menu

document.getElementById('Tinh').onclick = function() {
    var chucNang = document.getElementById('yeuCau').value*1
    switch(chucNang) {
        case 1: {
            ketQua =  doHomework1();
            console.log(ketQua)
        } break;
        case 2: {
            ketQua =  doHomework2();
            console.log(ketQua)
        } break;
        case 3: {
            ketQua =  doHomework3();
            console.log(ketQua)
        } break;
        case 4: {
            ketQua =  doHomework4();
            console.log(ketQua)
        } break;
        case 5: {
            ketQua =  doHomework5();
            console.log(ketQua)
        } break;
        case 6: {
            ketQua =  doHomework6();
            console.log(ketQua)
        } break;
        case 7: {
            ketQua =  doHomework7();
            console.log(ketQua)
        } break;
        case 8: {
            ketQua =  doHomework8();
            console.log(ketQua)
        } break;
        case 9: {
            ketQua =  doHomework9();
            console.log(ketQua)
        } break;
        case 10: {
            ketQua =  doHomework10();
            console.log(ketQua)
        } break;
        default: {
            alert('Xin mời bạn chọn chức năng')
        }
    }
}
// chuc nang 1: Tổng các số dương trong mảng.
function doHomework1() {
    let tong = 0;
    const tongSoDuong = arr.filter(function(item) {
    if (item >0) {
        tong += item
    }
})
let ketQua =   document.getElementById('result').innerHTML = tong
    return ketQua;
}
// chuc nang 2: Đếm có bao nhiêu số dương trong mảng.
function doHomework2() {
    count = 0;
    const demSo = arr.filter(function(number) {
        if (number > 0) {
        count++
        }
    }); 
    let ketQua =   document.getElementById('result').innerHTML = count
    return ketQua
}
// chuc nang 3: Tìm số nhỏ nhất trong mảng.
function doHomework3() {
    const min = Math.min(...arr)
    let ketQua =   document.getElementById('result').innerHTML = min
    return ketQua;
}
// chuc nang 4: Tìm số dương nhỏ nhất trong mảng.
function doHomework4() {
    const newArr = arr.filter(function(item){
        return item > 0;
    });
    const minDuong = Math.min(...newArr);
    let ketQua =   document.getElementById('result').innerHTML = minDuong;
    return ketQua;
}
// chuc nang 5: Tìm số chẵn cuối cùng trong mảng. Nếu mảng không có giá trị chắn thì trả về -1
function doHomework5() {
    let ketQua
    const newArr = arr.filter(function(item) {
        return item % 2 == 0;
    })
    if (newArr == 0) {
        ketQua = -1;
    } else {
        ketQua = Math.max((newArr[newArr.length-1]));
    }
    var result = document.getElementById('result').innerHTML = ketQua
    return result;
};
// chuc nang 6: Đổi chỗ 2 giá trị trong mảng theo vị trí (cho nhập vào 2 vị trí muốn đổi chỗ giá trị)
function doHomework6() {
    let value_1 = document.getElementById('chucNang__bai6-1').value*1
    let value_2 = document.getElementById('chucNang__bai6-2').value*1
    let assign = 0
    if (value_1 < 0 || value_2 < 0) {
        alert("Xin mời bạn chọn vị trí muốn thay đổi ** <||= 0");
    } else if (value_1 > (arr.length - 1) || value_2 > (arr.length -1)) {
        alert("Vị trí bạn muốn thay đổi lớn hơn số index của mảng");
    } else {
        for (var i = 0; i < arr.length; i++) {
            if (value_1 == i) {
                for(var j = 0; j < arr.length; j++) {
                    if(value_2 == j) {
                        assign = arr[i]
                        arr[i] = arr[j]
                        arr[j] = assign
                    }
                }
            }
        }
    }
    let ketQua =   document.getElementById('result').innerHTML = arr
   return ketQua;
}
// chức năng 7: Sắp xếp mảng theo thứ tự tăng dần.
function doHomework7() {
    const sortArr = arr.sort(function(a, b) {
        return a - b;
    })
    let ketQua =   document.getElementById('result').innerHTML = sortArr
    return ketQua
}

// Chức năng 8: Tìm số nguyên tố đầu tiên trong mảng. Nếu mảng không có số nguyên tố thì trả về -1.
function doHomework8() {
    let ketQua
    const newArr = arr.filter(function(num) {
        if(num < 2) {
            return false;
        } else if (num == 2){
            return true;
        } else {
            for (var i = 2; i < num; i++) {
                if (num % i === 0){
                    return false
                }
            }
            return true;
        }
    })
    if (newArr == 0) {
        ketQua = document.getElementById('result').innerHTML = -1
        return ketQua;
    } else {
         ketQua =   document.getElementById('result').innerHTML =`Số nguyên tố đầu tiên: ${newArr[0]} ` 

        return ketQua;
    }
}


//Chức năng 9: Nhập thêm 1 mảng số thực, tùm xem trong mảng có bao nhiêu số nguyên?
function doHomework9() {
    let tongArray =  arr.concat(arr_9)
    let count = 0
    let kiemTraTongArray = tongArray.filter(function(num) {
        if(Number.isInteger(num) == true) {
            count++
        }
    })
    ketQua = document.getElementById('result').innerHTML = `Tổng số nguyên: ${count}`
    return ketQua;
}


// Chức năng 10: So sánh số lượng số dương và số lượng số âm xem số nào nhiều hơn.
function doHomework10() {
    let soDuong = 0;
    let soAm = 0;
    let ketQua
    const newArr = arr.filter(function(num) {
        if (num > 0) {
            soDuong++;
        }
        return soDuong
    })
    const lastArr = arr.filter(function(item) {
        if(item < 0) {
            soAm++
        }
        return soAm
    })
    if (newArr > lastArr) {
        ketQua =   document.getElementById('result').innerHTML =`Số dương nhiều hơn` 
    } else {
        ketQua =   document.getElementById('result').innerHTML =`Số âm nhiều hơn` 
    }
    return ketQua;
 }